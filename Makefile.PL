use 5.006;
use strict;
use warnings;
use ExtUtils::MakeMaker;

WriteMakefile(
    NAME                => 'Geo::EZLocate',
    AUTHOR              => q{Mark Wells <mark@freeside.biz>},
    VERSION_FROM        => 'EZLocate.pm',
    ABSTRACT_FROM       => 'EZLocate.pm',
    ($ExtUtils::MakeMaker::VERSION >= 6.3002
      ? ('LICENSE'=> 'perl')
      : ()),
    PL_FILES            => {},
    PREREQ_PM => {
        'Test::More'                 => 0,
        'Class::Std::Fast::Storable' => 0,
        'SOAP::WSDL'                 => 0,
    },
    dist                => { COMPRESS => 'gzip -9f', SUFFIX => 'gz', },
    clean               => { FILES => 'Geo-EZLocate-*' },
);
