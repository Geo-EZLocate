#!perl -T

use Test::More tests => 1;

BEGIN {
    use_ok( 'Geo::EZLocate' ) || print "Bail out!\n";
}

diag( "Testing Geo::EZLocate $Geo::EZLocate::VERSION, Perl $], $^X" );
