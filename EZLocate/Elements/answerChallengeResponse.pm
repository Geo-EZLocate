
package Geo::EZLocate::Elements::answerChallengeResponse;
use strict;
use warnings;

{ # BLOCK to scope variables

sub get_xmlns { 'http://ezlocate.na.teleatlas.com/Authentication.xsd1' }

__PACKAGE__->__set_name('answerChallengeResponse');
__PACKAGE__->__set_nillable();
__PACKAGE__->__set_minOccurs();
__PACKAGE__->__set_maxOccurs();
__PACKAGE__->__set_ref();

use base qw(
    SOAP::WSDL::XSD::Typelib::Element
    SOAP::WSDL::XSD::Typelib::ComplexType
);

our $XML_ATTRIBUTE_CLASS;
undef $XML_ATTRIBUTE_CLASS;

sub __get_attr_class {
    return $XML_ATTRIBUTE_CLASS;
}

use Class::Std::Fast::Storable constructor => 'none';
use base qw(SOAP::WSDL::XSD::Typelib::ComplexType);

Class::Std::initialize();

{ # BLOCK to scope variables

my %resultCode_of :ATTR(:get<resultCode>);
my %credential_of :ATTR(:get<credential>);

__PACKAGE__->_factory(
    [ qw(        resultCode
        credential

    ) ],
    {
        'resultCode' => \%resultCode_of,
        'credential' => \%credential_of,
    },
    {
        'resultCode' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
        'credential' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
    },
    {

        'resultCode' => 'resultCode',
        'credential' => 'credential',
    }
);

} # end BLOCK






} # end of BLOCK



1;


=pod

=head1 NAME

Geo::EZLocate::Elements::answerChallengeResponse

=head1 DESCRIPTION

Perl data type class for the XML Schema defined element
answerChallengeResponse from the namespace http://ezlocate.na.teleatlas.com/Authentication.xsd1.







=head1 PROPERTIES

The following properties may be accessed using get_PROPERTY / set_PROPERTY
methods:

=over

=item * resultCode

 $element->set_resultCode($data);
 $element->get_resultCode();




=item * credential

 $element->set_credential($data);
 $element->get_credential();





=back


=head1 METHODS

=head2 new

 my $element = Geo::EZLocate::Elements::answerChallengeResponse->new($data);

Constructor. The following data structure may be passed to new():

 {
   resultCode =>  $some_value, # int
   credential =>  $some_value, # int
 },

=head1 AUTHOR

Generated by SOAP::WSDL

=cut

