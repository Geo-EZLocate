
package Geo::EZLocate::Elements::testCredentialResponse;
use strict;
use warnings;

{ # BLOCK to scope variables

sub get_xmlns { 'http://ezlocate.na.teleatlas.com/Authentication.xsd1' }

__PACKAGE__->__set_name('testCredentialResponse');
__PACKAGE__->__set_nillable();
__PACKAGE__->__set_minOccurs();
__PACKAGE__->__set_maxOccurs();
__PACKAGE__->__set_ref();

use base qw(
    SOAP::WSDL::XSD::Typelib::Element
    SOAP::WSDL::XSD::Typelib::ComplexType
);

our $XML_ATTRIBUTE_CLASS;
undef $XML_ATTRIBUTE_CLASS;

sub __get_attr_class {
    return $XML_ATTRIBUTE_CLASS;
}

use Class::Std::Fast::Storable constructor => 'none';
use base qw(SOAP::WSDL::XSD::Typelib::ComplexType);

Class::Std::initialize();

{ # BLOCK to scope variables

my %resultCode_of :ATTR(:get<resultCode>);
my %user_of :ATTR(:get<user>);
my %password_of :ATTR(:get<password>);
my %expiration_of :ATTR(:get<expiration>);

__PACKAGE__->_factory(
    [ qw(        resultCode
        user
        password
        expiration

    ) ],
    {
        'resultCode' => \%resultCode_of,
        'user' => \%user_of,
        'password' => \%password_of,
        'expiration' => \%expiration_of,
    },
    {
        'resultCode' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
        'user' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
        'password' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
        'expiration' => 'SOAP::WSDL::XSD::Typelib::Builtin::long',
    },
    {

        'resultCode' => 'resultCode',
        'user' => 'user',
        'password' => 'password',
        'expiration' => 'expiration',
    }
);

} # end BLOCK






} # end of BLOCK



1;


=pod

=head1 NAME

Geo::EZLocate::Elements::testCredentialResponse

=head1 DESCRIPTION

Perl data type class for the XML Schema defined element
testCredentialResponse from the namespace http://ezlocate.na.teleatlas.com/Authentication.xsd1.







=head1 PROPERTIES

The following properties may be accessed using get_PROPERTY / set_PROPERTY
methods:

=over

=item * resultCode

 $element->set_resultCode($data);
 $element->get_resultCode();




=item * user

 $element->set_user($data);
 $element->get_user();




=item * password

 $element->set_password($data);
 $element->get_password();




=item * expiration

 $element->set_expiration($data);
 $element->get_expiration();





=back


=head1 METHODS

=head2 new

 my $element = Geo::EZLocate::Elements::testCredentialResponse->new($data);

Constructor. The following data structure may be passed to new():

 {
   resultCode =>  $some_value, # int
   user =>  $some_value, # string
   password =>  $some_value, # string
   expiration =>  $some_value, # long
 },

=head1 AUTHOR

Generated by SOAP::WSDL

=cut

