package Geo::EZLocate::Types::Record;
use strict;
use warnings;


__PACKAGE__->_set_element_form_qualified(0);

sub get_xmlns { 'http://ezlocate.na.teleatlas.com/Geocoding.xsd1' };

our $XML_ATTRIBUTE_CLASS;
undef $XML_ATTRIBUTE_CLASS;

sub __get_attr_class {
    return $XML_ATTRIBUTE_CLASS;
}

use Class::Std::Fast::Storable constructor => 'none';
use base qw(SOAP::WSDL::XSD::Typelib::ComplexType);

Class::Std::initialize();

{ # BLOCK to scope variables

my %nv_of :ATTR(:get<nv>);

__PACKAGE__->_factory(
    [ qw(        nv

    ) ],
    {
        'nv' => \%nv_of,
    },
    {
        'nv' => 'Geo::EZLocate::Types::NameValue',
    },
    {

        'nv' => 'nv',
    }
);

} # end BLOCK







1;


=pod

=head1 NAME

Geo::EZLocate::Types::Record

=head1 DESCRIPTION

Perl data type class for the XML Schema defined complexType
Record from the namespace http://ezlocate.na.teleatlas.com/Geocoding.xsd1.






=head2 PROPERTIES

The following properties may be accessed using get_PROPERTY / set_PROPERTY
methods:

=over

=item * nv




=back


=head1 METHODS

=head2 new

Constructor. The following data structure may be passed to new():

 { # Geo::EZLocate::Types::Record
   nv =>  { # Geo::EZLocate::Types::NameValue
     name =>  $some_value, # string
     value =>  $some_value, # string
   },
 },




=head1 AUTHOR

Generated by SOAP::WSDL

=cut

