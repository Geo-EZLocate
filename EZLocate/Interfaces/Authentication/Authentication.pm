package Geo::EZLocate::Interfaces::Authentication::Authentication;
use strict;
use warnings;
use Class::Std::Fast::Storable;
use Scalar::Util qw(blessed);
use base qw(SOAP::WSDL::Client::Base);

# only load if it hasn't been loaded before
require Geo::EZLocate::Typemaps::Authentication
    if not Geo::EZLocate::Typemaps::Authentication->can('get_class');

sub START {
    $_[0]->set_proxy('http://mmezl.teleatlas.com/axis/services/Authentication') if not $_[2]->{proxy};
    $_[0]->set_class_resolver('Geo::EZLocate::Typemaps::Authentication')
        if not $_[2]->{class_resolver};

    $_[0]->set_prefix($_[2]->{use_prefix}) if exists $_[2]->{use_prefix};
}

sub requestChallenge {
    my ($self, $body, $header) = @_;
    die "requestChallenge must be called as object method (\$self is <$self>)" if not blessed($self);
    return $self->SUPER::call({
        operation => 'requestChallenge',
        soap_action => 'Authentication:AuthenticationPortType#requestChallenge',
        style => 'document',
        body => {
            

           'use'            => 'literal',
            namespace       => 'http://schemas.xmlsoap.org/wsdl/soap/',
            encodingStyle   => '',
            parts           =>  [qw( Geo::EZLocate::Elements::requestChallenge )],
        },
        header => {
            
        },
        headerfault => {
            
        }
    }, $body, $header);
}


sub answerChallenge {
    my ($self, $body, $header) = @_;
    die "answerChallenge must be called as object method (\$self is <$self>)" if not blessed($self);
    return $self->SUPER::call({
        operation => 'answerChallenge',
        soap_action => 'Authentication:AuthenticationPortType#answerChallenge',
        style => 'document',
        body => {
            

           'use'            => 'literal',
            namespace       => 'http://schemas.xmlsoap.org/wsdl/soap/',
            encodingStyle   => '',
            parts           =>  [qw( Geo::EZLocate::Elements::answerChallenge )],
        },
        header => {
            
        },
        headerfault => {
            
        }
    }, $body, $header);
}


sub invalidateCredential {
    my ($self, $body, $header) = @_;
    die "invalidateCredential must be called as object method (\$self is <$self>)" if not blessed($self);
    return $self->SUPER::call({
        operation => 'invalidateCredential',
        soap_action => 'Authentication:AuthenticationPortType#invalidateCredential',
        style => 'document',
        body => {
            

           'use'            => 'literal',
            namespace       => 'http://schemas.xmlsoap.org/wsdl/soap/',
            encodingStyle   => '',
            parts           =>  [qw( Geo::EZLocate::Elements::invalidateCredential )],
        },
        header => {
            
        },
        headerfault => {
            
        }
    }, $body, $header);
}


sub testCredential {
    my ($self, $body, $header) = @_;
    die "testCredential must be called as object method (\$self is <$self>)" if not blessed($self);
    return $self->SUPER::call({
        operation => 'testCredential',
        soap_action => 'Authentication:AuthenticationPortType#testCredential',
        style => 'document',
        body => {
            

           'use'            => 'literal',
            namespace       => 'http://schemas.xmlsoap.org/wsdl/soap/',
            encodingStyle   => '',
            parts           =>  [qw( Geo::EZLocate::Elements::testCredential )],
        },
        header => {
            
        },
        headerfault => {
            
        }
    }, $body, $header);
}




1;



__END__

=pod

=head1 NAME

Geo::EZLocate::Interfaces::Authentication::Authentication - SOAP Interface for the Authentication Web Service

=head1 SYNOPSIS

 use Geo::EZLocate::Interfaces::Authentication::Authentication;
 my $interface = Geo::EZLocate::Interfaces::Authentication::Authentication->new();

 my $response;
 $response = $interface->requestChallenge();
 $response = $interface->answerChallenge();
 $response = $interface->invalidateCredential();
 $response = $interface->testCredential();



=head1 DESCRIPTION

SOAP Interface for the Authentication web service
located at http://mmezl.teleatlas.com/axis/services/Authentication.

=head1 SERVICE Authentication



=head2 Port Authentication



=head1 METHODS

=head2 General methods

=head3 new

Constructor.

All arguments are forwarded to L<SOAP::WSDL::Client|SOAP::WSDL::Client>.

=head2 SOAP Service methods

Method synopsis is displayed with hash refs as parameters.

The commented class names in the method's parameters denote that objects
of the corresponding class can be passed instead of the marked hash ref.

You may pass any combination of objects, hash and list refs to these
methods, as long as you meet the structure.

List items (i.e. multiple occurences) are not displayed in the synopsis.
You may generally pass a list ref of hash refs (or objects) instead of a hash
ref - this may result in invalid XML if used improperly, though. Note that
SOAP::WSDL always expects list references at maximum depth position.

XML attributes are not displayed in this synopsis and cannot be set using
hash refs. See the respective class' documentation for additional information.



=head3 requestChallenge



Returns a L<Geo::EZLocate::Elements::requestChallengeResponse|Geo::EZLocate::Elements::requestChallengeResponse> object.

 $response = $interface->requestChallenge( {
    userName =>  $some_value, # string
    minutesValid =>  $some_value, # int
  },,
 );

=head3 answerChallenge



Returns a L<Geo::EZLocate::Elements::answerChallengeResponse|Geo::EZLocate::Elements::answerChallengeResponse> object.

 $response = $interface->answerChallenge( {
    encryptedResponse =>  $some_value, # int
    originalChallenge =>  $some_value, # int
  },,
 );

=head3 invalidateCredential



Returns a L<Geo::EZLocate::Elements::invalidateCredentialResponse|Geo::EZLocate::Elements::invalidateCredentialResponse> object.

 $response = $interface->invalidateCredential( {
    credential =>  $some_value, # int
  },,
 );

=head3 testCredential



Returns a L<Geo::EZLocate::Elements::testCredentialResponse|Geo::EZLocate::Elements::testCredentialResponse> object.

 $response = $interface->testCredential( {
    ipAddress =>  $some_value, # string
    credential =>  $some_value, # int
    serverCred =>  $some_value, # int
  },,
 );



=head1 AUTHOR

Generated by SOAP::WSDL on Fri Nov  2 14:12:51 2012

=cut
