package Geo::EZLocate::Interfaces::Geocoding::Geocoding;
use strict;
use warnings;
use Class::Std::Fast::Storable;
use Scalar::Util qw(blessed);
use base qw(SOAP::WSDL::Client::Base);

# only load if it hasn't been loaded before
require Geo::EZLocate::Typemaps::Geocoding
    if not Geo::EZLocate::Typemaps::Geocoding->can('get_class');

sub START {
    $_[0]->set_proxy('http://mmezl.teleatlas.com/axis/services/Geocoding') if not $_[2]->{proxy};
    $_[0]->set_class_resolver('Geo::EZLocate::Typemaps::Geocoding')
        if not $_[2]->{class_resolver};

    $_[0]->set_prefix($_[2]->{use_prefix}) if exists $_[2]->{use_prefix};
}

sub getServices {
    my ($self, $body, $header) = @_;
    die "getServices must be called as object method (\$self is <$self>)" if not blessed($self);
    return $self->SUPER::call({
        operation => 'getServices',
        soap_action => 'Geocoding:GeocodingPortType#getServices',
        style => 'document',
        body => {
            

           'use'            => 'literal',
            namespace       => 'http://schemas.xmlsoap.org/wsdl/soap/',
            encodingStyle   => '',
            parts           =>  [qw( Geo::EZLocate::Elements::getServices )],
        },
        header => {
            
        },
        headerfault => {
            
        }
    }, $body, $header);
}


sub getServiceDescription {
    my ($self, $body, $header) = @_;
    die "getServiceDescription must be called as object method (\$self is <$self>)" if not blessed($self);
    return $self->SUPER::call({
        operation => 'getServiceDescription',
        soap_action => 'Geocoding:GeocodingPortType#getServiceDescription',
        style => 'document',
        body => {
            

           'use'            => 'literal',
            namespace       => 'http://schemas.xmlsoap.org/wsdl/soap/',
            encodingStyle   => '',
            parts           =>  [qw( Geo::EZLocate::Elements::getServiceDescription )],
        },
        header => {
            
        },
        headerfault => {
            
        }
    }, $body, $header);
}


sub findAddress {
    my ($self, $body, $header) = @_;
    die "findAddress must be called as object method (\$self is <$self>)" if not blessed($self);
    return $self->SUPER::call({
        operation => 'findAddress',
        soap_action => 'Geocoding:GeocodingPortType#findAddress',
        style => 'document',
        body => {
            

           'use'            => 'literal',
            namespace       => 'http://schemas.xmlsoap.org/wsdl/soap/',
            encodingStyle   => '',
            parts           =>  [qw( Geo::EZLocate::Elements::findAddress )],
        },
        header => {
            
        },
        headerfault => {
            
        }
    }, $body, $header);
}


sub findMultiAddress {
    my ($self, $body, $header) = @_;
    die "findMultiAddress must be called as object method (\$self is <$self>)" if not blessed($self);
    return $self->SUPER::call({
        operation => 'findMultiAddress',
        soap_action => 'Geocoding:GeocodingPortType#findMultiAddress',
        style => 'document',
        body => {
            

           'use'            => 'literal',
            namespace       => 'http://schemas.xmlsoap.org/wsdl/soap/',
            encodingStyle   => '',
            parts           =>  [qw( Geo::EZLocate::Elements::findMultiAddress )],
        },
        header => {
            
        },
        headerfault => {
            
        }
    }, $body, $header);
}




1;



__END__

=pod

=head1 NAME

Geo::EZLocate::Interfaces::Geocoding::Geocoding - SOAP Interface for the Geocoding Web Service

=head1 SYNOPSIS

 use Geo::EZLocate::Interfaces::Geocoding::Geocoding;
 my $interface = Geo::EZLocate::Interfaces::Geocoding::Geocoding->new();

 my $response;
 $response = $interface->getServices();
 $response = $interface->getServiceDescription();
 $response = $interface->findAddress();
 $response = $interface->findMultiAddress();



=head1 DESCRIPTION

SOAP Interface for the Geocoding web service
located at http://mmezl.teleatlas.com/axis/services/Geocoding.

=head1 SERVICE Geocoding



=head2 Port Geocoding



=head1 METHODS

=head2 General methods

=head3 new

Constructor.

All arguments are forwarded to L<SOAP::WSDL::Client|SOAP::WSDL::Client>.

=head2 SOAP Service methods

Method synopsis is displayed with hash refs as parameters.

The commented class names in the method's parameters denote that objects
of the corresponding class can be passed instead of the marked hash ref.

You may pass any combination of objects, hash and list refs to these
methods, as long as you meet the structure.

List items (i.e. multiple occurences) are not displayed in the synopsis.
You may generally pass a list ref of hash refs (or objects) instead of a hash
ref - this may result in invalid XML if used improperly, though. Note that
SOAP::WSDL always expects list references at maximum depth position.

XML attributes are not displayed in this synopsis and cannot be set using
hash refs. See the respective class' documentation for additional information.



=head3 getServices



Returns a L<Geo::EZLocate::Elements::getServicesResponse|Geo::EZLocate::Elements::getServicesResponse> object.

 $response = $interface->getServices( {
    identity =>  $some_value, # int
  },,
 );

=head3 getServiceDescription



Returns a L<Geo::EZLocate::Elements::getServiceDescriptionResponse|Geo::EZLocate::Elements::getServiceDescriptionResponse> object.

 $response = $interface->getServiceDescription( {
    service =>  $some_value, # string
    identity =>  $some_value, # int
  },,
 );

=head3 findAddress



Returns a L<Geo::EZLocate::Elements::findAddressResponse|Geo::EZLocate::Elements::findAddressResponse> object.

 $response = $interface->findAddress( {
    service =>  $some_value, # string
    input =>  { # Geo::EZLocate::Types::Record
      nv =>  { # Geo::EZLocate::Types::NameValue
        name =>  $some_value, # string
        value =>  $some_value, # string
      },
    },
    identity =>  $some_value, # int
  },,
 );

=head3 findMultiAddress



Returns a L<Geo::EZLocate::Elements::findMultiAddressResponse|Geo::EZLocate::Elements::findMultiAddressResponse> object.

 $response = $interface->findMultiAddress( {
    service =>  $some_value, # string
    inputs =>  { # Geo::EZLocate::Types::RecordSequence
      record =>  { # Geo::EZLocate::Types::Record
        nv =>  { # Geo::EZLocate::Types::NameValue
          name =>  $some_value, # string
          value =>  $some_value, # string
        },
      },
    },
    identity =>  $some_value, # int
  },,
 );



=head1 AUTHOR

Generated by SOAP::WSDL on Fri Nov  2 14:12:17 2012

=cut
