package Geo::EZLocate::Interfaces::EZClient::EZClient;
use strict;
use warnings;
use Class::Std::Fast::Storable;
use Scalar::Util qw(blessed);
use base qw(SOAP::WSDL::Client::Base);

# only load if it hasn't been loaded before
require Geo::EZLocate::Typemaps::EZClient
    if not Geo::EZLocate::Typemaps::EZClient->can('get_class');

sub START {
    $_[0]->set_proxy('http://mmezl.teleatlas.com/axis/services/EZClient') if not $_[2]->{proxy};
    $_[0]->set_class_resolver('Geo::EZLocate::Typemaps::EZClient')
        if not $_[2]->{class_resolver};

    $_[0]->set_prefix($_[2]->{use_prefix}) if exists $_[2]->{use_prefix};
}

sub getMOTD {
    my ($self, $body, $header) = @_;
    die "getMOTD must be called as object method (\$self is <$self>)" if not blessed($self);
    return $self->SUPER::call({
        operation => 'getMOTD',
        soap_action => 'EZClient:EZClientPortType#getMOTD',
        style => 'document',
        body => {
            

           'use'            => 'literal',
            namespace       => 'http://schemas.xmlsoap.org/wsdl/soap/',
            encodingStyle   => '',
            parts           =>  [qw( Geo::EZLocate::Elements::getMOTD )],
        },
        header => {
            
        },
        headerfault => {
            
        }
    }, $body, $header);
}


sub getClientInfo {
    my ($self, $body, $header) = @_;
    die "getClientInfo must be called as object method (\$self is <$self>)" if not blessed($self);
    return $self->SUPER::call({
        operation => 'getClientInfo',
        soap_action => 'EZClient:EZClientPortType#getClientInfo',
        style => 'document',
        body => {
            

           'use'            => 'literal',
            namespace       => 'http://schemas.xmlsoap.org/wsdl/soap/',
            encodingStyle   => '',
            parts           =>  [qw( Geo::EZLocate::Elements::getClientInfo )],
        },
        header => {
            
        },
        headerfault => {
            
        }
    }, $body, $header);
}


sub getAccountInfo {
    my ($self, $body, $header) = @_;
    die "getAccountInfo must be called as object method (\$self is <$self>)" if not blessed($self);
    return $self->SUPER::call({
        operation => 'getAccountInfo',
        soap_action => 'EZClient:EZClientPortType#getAccountInfo',
        style => 'document',
        body => {
            

           'use'            => 'literal',
            namespace       => 'http://schemas.xmlsoap.org/wsdl/soap/',
            encodingStyle   => '',
            parts           =>  [qw( Geo::EZLocate::Elements::getAccountInfo )],
        },
        header => {
            
        },
        headerfault => {
            
        }
    }, $body, $header);
}


sub getRPS {
    my ($self, $body, $header) = @_;
    die "getRPS must be called as object method (\$self is <$self>)" if not blessed($self);
    return $self->SUPER::call({
        operation => 'getRPS',
        soap_action => 'EZClient:EZClientPortType#getRPS',
        style => 'document',
        body => {
            

           'use'            => 'literal',
            namespace       => 'http://schemas.xmlsoap.org/wsdl/soap/',
            encodingStyle   => '',
            parts           =>  [qw( Geo::EZLocate::Elements::getRPS )],
        },
        header => {
            
        },
        headerfault => {
            
        }
    }, $body, $header);
}




1;



__END__

=pod

=head1 NAME

Geo::EZLocate::Interfaces::EZClient::EZClient - SOAP Interface for the EZClient Web Service

=head1 SYNOPSIS

 use Geo::EZLocate::Interfaces::EZClient::EZClient;
 my $interface = Geo::EZLocate::Interfaces::EZClient::EZClient->new();

 my $response;
 $response = $interface->getMOTD();
 $response = $interface->getClientInfo();
 $response = $interface->getAccountInfo();
 $response = $interface->getRPS();



=head1 DESCRIPTION

SOAP Interface for the EZClient web service
located at http://mmezl.teleatlas.com/axis/services/EZClient.

=head1 SERVICE EZClient



=head2 Port EZClient



=head1 METHODS

=head2 General methods

=head3 new

Constructor.

All arguments are forwarded to L<SOAP::WSDL::Client|SOAP::WSDL::Client>.

=head2 SOAP Service methods

Method synopsis is displayed with hash refs as parameters.

The commented class names in the method's parameters denote that objects
of the corresponding class can be passed instead of the marked hash ref.

You may pass any combination of objects, hash and list refs to these
methods, as long as you meet the structure.

List items (i.e. multiple occurences) are not displayed in the synopsis.
You may generally pass a list ref of hash refs (or objects) instead of a hash
ref - this may result in invalid XML if used improperly, though. Note that
SOAP::WSDL always expects list references at maximum depth position.

XML attributes are not displayed in this synopsis and cannot be set using
hash refs. See the respective class' documentation for additional information.



=head3 getMOTD



Returns a L<Geo::EZLocate::Elements::getMOTDResponse|Geo::EZLocate::Elements::getMOTDResponse> object.

 $response = $interface->getMOTD( {
  },,
 );

=head3 getClientInfo



Returns a L<Geo::EZLocate::Elements::getClientInfoResponse|Geo::EZLocate::Elements::getClientInfoResponse> object.

 $response = $interface->getClientInfo( {
    majorVersion =>  $some_value, # int
    minorVersion =>  $some_value, # int
  },,
 );

=head3 getAccountInfo



Returns a L<Geo::EZLocate::Elements::getAccountInfoResponse|Geo::EZLocate::Elements::getAccountInfoResponse> object.

 $response = $interface->getAccountInfo( {
    identity =>  $some_value, # int
  },,
 );

=head3 getRPS



Returns a L<Geo::EZLocate::Elements::getRPSResponse|Geo::EZLocate::Elements::getRPSResponse> object.

 $response = $interface->getRPS( {
    identity =>  $some_value, # int
  },,
 );



=head1 AUTHOR

Generated by SOAP::WSDL on Fri Nov  2 14:13:07 2012

=cut
