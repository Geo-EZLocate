
package Geo::EZLocate::Typemaps::Geocoding;
use strict;
use warnings;

our $typemap_1 = {
               'getServiceDescriptionResponse/inputs' => 'Geo::EZLocate::Types::Record',
               'findMultiAddressResponse/results/sequence/resultCode' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
               'getServicesResponse/services/nv/value' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'findAddress/input/nv/name' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'Fault/faultcode' => 'SOAP::WSDL::XSD::Typelib::Builtin::anyURI',
               'findMultiAddress/inputs/record/nv/name' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'getServices' => 'Geo::EZLocate::Elements::getServices',
               'getServicesResponse/services/nv' => 'Geo::EZLocate::Types::NameValue',
               'findAddressResponse' => 'Geo::EZLocate::Elements::findAddressResponse',
               'findAddress/input/nv' => 'Geo::EZLocate::Types::NameValue',
               'getServicesResponse/services/nv/name' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'getServiceDescriptionResponse' => 'Geo::EZLocate::Elements::getServiceDescriptionResponse',
               'getServiceDescriptionResponse/matchTypes' => 'Geo::EZLocate::Types::MatchTypeSequence',
               'findAddressResponse/result/mAttributes/nv/name' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'getServiceDescriptionResponse/matchTypes/types' => 'Geo::EZLocate::Types::MatchType',
               'findMultiAddressResponse/results/sequence/mAttributes/nv' => 'Geo::EZLocate::Types::NameValue',
               'findMultiAddressResponse' => 'Geo::EZLocate::Elements::findMultiAddressResponse',
               'findMultiAddress/inputs/record/nv/value' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'getServiceDescriptionResponse/outputs/fields/description' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'findAddress' => 'Geo::EZLocate::Elements::findAddress',
               'getServices/identity' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
               'findMultiAddressResponse/results' => 'Geo::EZLocate::Types::GeocodeSequence',
               'findMultiAddressResponse/results/sequence' => 'Geo::EZLocate::Types::Geocode',
               'getServiceDescriptionResponse/inputs/nv/name' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'findMultiAddressResponse/resultCode' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
               'findAddress/identity' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
               'findAddressResponse/result/mAttributes/nv/value' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'Fault/faultstring' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'getServiceDescriptionResponse/outputs' => 'Geo::EZLocate::Types::OutputFieldSequence',
               'findAddress/input' => 'Geo::EZLocate::Types::Record',
               'getServiceDescriptionResponse/description' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'findAddress/service' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'findAddressResponse/resultCode' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
               'getServicesResponse' => 'Geo::EZLocate::Elements::getServicesResponse',
               'Fault/detail' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'findMultiAddress/inputs' => 'Geo::EZLocate::Types::RecordSequence',
               'findAddressResponse/result' => 'Geo::EZLocate::Types::Geocode',
               'findAddressResponse/result/mAttributes/nv' => 'Geo::EZLocate::Types::NameValue',
               'findMultiAddressResponse/results/sequence/mAttributes' => 'Geo::EZLocate::Types::Record',
               'getServiceDescriptionResponse/inputs/nv/value' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'findMultiAddress/inputs/record' => 'Geo::EZLocate::Types::Record',
               'findAddressResponse/result/mAttributes' => 'Geo::EZLocate::Types::Record',
               'getServiceDescription/identity' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
               'findMultiAddress/identity' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
               'getServiceDescription' => 'Geo::EZLocate::Elements::getServiceDescription',
               'findMultiAddress/service' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'findMultiAddress/inputs/record/nv' => 'Geo::EZLocate::Types::NameValue',
               'getServicesResponse/services' => 'Geo::EZLocate::Types::Record',
               'findAddressResponse/result/resultCode' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
               'findMultiAddress' => 'Geo::EZLocate::Elements::findMultiAddress',
               'getServiceDescriptionResponse/matchTypeName' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'getServiceDescription/service' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'findMultiAddressResponse/results/sequence/mAttributes/nv/name' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'findMultiAddressResponse/results/sequence/mAttributes/nv/value' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'getServiceDescriptionResponse/outputs/fields/type' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
               'getServiceDescriptionResponse/outputs/fields/name' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'getServicesResponse/resultCode' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
               'getServiceDescriptionResponse/resultCode' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
               'getServiceDescriptionResponse/matchTypes/types/description' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'getServiceDescriptionResponse/outputs/fields' => 'Geo::EZLocate::Types::OutputField',
               'getServiceDescriptionResponse/matchTypes/types/id' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
               'Fault' => 'SOAP::WSDL::SOAP::Typelib::Fault11',
               'findAddress/input/nv/value' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'Fault/faultactor' => 'SOAP::WSDL::XSD::Typelib::Builtin::token',
               'getServiceDescriptionResponse/inputs/nv' => 'Geo::EZLocate::Types::NameValue',
               'getServiceDescriptionResponse/countryCode' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'getServiceDescriptionResponse/matchTypes/types/name' => 'SOAP::WSDL::XSD::Typelib::Builtin::string'
             };
;

sub get_class {
  my $name = join '/', @{ $_[1] };
  return $typemap_1->{ $name };
}

sub get_typemap {
    return $typemap_1;
}

1;

__END__

__END__

=pod

=head1 NAME

Geo::EZLocate::Typemaps::Geocoding - typemap for Geocoding

=head1 DESCRIPTION

Typemap created by SOAP::WSDL for map-based SOAP message parsers.

=cut

