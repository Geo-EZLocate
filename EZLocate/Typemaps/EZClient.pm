
package Geo::EZLocate::Typemaps::EZClient;
use strict;
use warnings;

our $typemap_1 = {
               'getRPSResponse' => 'Geo::EZLocate::Elements::getRPSResponse',
               'getMOTDResponse' => 'Geo::EZLocate::Elements::getMOTDResponse',
               'getAccountInfo' => 'Geo::EZLocate::Elements::getAccountInfo',
               'getAccountInfoResponse/accountType' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'getAccountInfoResponse' => 'Geo::EZLocate::Elements::getAccountInfoResponse',
               'getClientInfo' => 'Geo::EZLocate::Elements::getClientInfo',
               'getAccountInfoResponse/actualRecords' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
               'Fault/faultcode' => 'SOAP::WSDL::XSD::Typelib::Builtin::anyURI',
               'getAccountInfoResponse/resultCode' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
               'getMOTDResponse/message' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'Fault/faultstring' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'getClientInfoResponse' => 'Geo::EZLocate::Elements::getClientInfoResponse',
               'getClientInfoResponse/safeToRun' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
               'getClientInfo/minorVersion' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
               'Fault' => 'SOAP::WSDL::SOAP::Typelib::Fault11',
               'getAccountInfoResponse/expiration' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'Fault/faultactor' => 'SOAP::WSDL::XSD::Typelib::Builtin::token',
               'getClientInfoResponse/message' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'getRPS/identity' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
               'Fault/detail' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'getAccountInfoResponse/maximumRecords' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
               'getClientInfo/majorVersion' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
               'getRPSResponse/recordsPerSecond' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
               'getMOTD' => 'Geo::EZLocate::Elements::getMOTD',
               'getAccountInfo/identity' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
               'getRPS' => 'Geo::EZLocate::Elements::getRPS'
             };
;

sub get_class {
  my $name = join '/', @{ $_[1] };
  return $typemap_1->{ $name };
}

sub get_typemap {
    return $typemap_1;
}

1;

__END__

__END__

=pod

=head1 NAME

Geo::EZLocate::Typemaps::EZClient - typemap for EZClient

=head1 DESCRIPTION

Typemap created by SOAP::WSDL for map-based SOAP message parsers.

=cut

