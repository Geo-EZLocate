
package Geo::EZLocate::Typemaps::Authentication;
use strict;
use warnings;

our $typemap_1 = {
               'invalidateCredentialResponse' => 'Geo::EZLocate::Elements::invalidateCredentialResponse',
               'requestChallenge' => 'Geo::EZLocate::Elements::requestChallenge',
               'testCredentialResponse/user' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'testCredentialResponse/resultCode' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
               'testCredential/ipAddress' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'answerChallenge' => 'Geo::EZLocate::Elements::answerChallenge',
               'requestChallengeResponse/resultCode' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
               'answerChallenge/encryptedResponse' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
               'Fault/faultcode' => 'SOAP::WSDL::XSD::Typelib::Builtin::anyURI',
               'invalidateCredential' => 'Geo::EZLocate::Elements::invalidateCredential',
               'answerChallengeResponse/resultCode' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
               'requestChallengeResponse/encryptedID' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
               'testCredential' => 'Geo::EZLocate::Elements::testCredential',
               'Fault/faultstring' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'testCredential/credential' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
               'testCredentialResponse/password' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'Fault' => 'SOAP::WSDL::SOAP::Typelib::Fault11',
               'answerChallenge/originalChallenge' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
               'Fault/faultactor' => 'SOAP::WSDL::XSD::Typelib::Builtin::token',
               'requestChallengeResponse' => 'Geo::EZLocate::Elements::requestChallengeResponse',
               'testCredentialResponse/expiration' => 'SOAP::WSDL::XSD::Typelib::Builtin::long',
               'Fault/detail' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'answerChallengeResponse/credential' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
               'testCredential/serverCred' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
               'answerChallengeResponse' => 'Geo::EZLocate::Elements::answerChallengeResponse',
               'testCredentialResponse' => 'Geo::EZLocate::Elements::testCredentialResponse',
               'requestChallenge/minutesValid' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
               'invalidateCredentialResponse/resultCode' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
               'requestChallenge/userName' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
               'invalidateCredential/credential' => 'SOAP::WSDL::XSD::Typelib::Builtin::int'
             };
;

sub get_class {
  my $name = join '/', @{ $_[1] };
  return $typemap_1->{ $name };
}

sub get_typemap {
    return $typemap_1;
}

1;

__END__

__END__

=pod

=head1 NAME

Geo::EZLocate::Typemaps::Authentication - typemap for Authentication

=head1 DESCRIPTION

Typemap created by SOAP::WSDL for map-based SOAP message parsers.

=cut

